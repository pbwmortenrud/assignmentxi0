const express = require('express');
const router = express.Router();
const modContinent = require("../models/handleContinents");
const modCountry = require("../models/handleCountries");
const modCity = require("../models/handleCities");

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Fragments of the World',
        subtitle: 'Playing with the World'
    });
});

router.get('/worldview', async function(req, res, next) {
    res.render('worldview', {
        title: 'Fragments of the World',
        subtitle: 'Start by Choosing a Continent'
    });
});

router.get('/continents', async function(req, res, next) {
    let continents = await modContinent.getContinents({}, {sort: {name: 1}});
    res.json(continents);
    
});

router.get('/continents/details', async function(req, res, next) {
    let countries = await modCountry.getCountries({}, {sort: {continent: 1}});
    res.render('cntrycont', {
        title: 'Details',
        countries: countries
    });
});

router.get('/continents/:cont', async function(req, res, next) {
    let countries = await modCountry.getCountries({continent: req.params.cont}, {sort: {name: 1}});
    res.json(countries);
});

router.get('/countries/:name', async function(req, res, next) {
    let countries = await modCountry.getCountries({name: req.params.name}, {sort: {name: 1}});
    res.json(countries);
});


router.get('/countries/', async function(req, res, next) {
    let countries = await modCountry.getCountries({}, {sort: {name: 1}});
    res.render('countryDisplay', {
        title: 'All Countries',
        countries: countries
    });
});

router.get('/countries/:name/details', async function(req, res, next) {
    let countries = await modCountry.getCountries({name: req.params.name}, {sort: {name: 1}});
    res.render('countryDisplay', {
        title: 'Details',
        countries: countries
    });
});

router.get('/cities/:countrycode', async function(req, res, next) {
    console.log('5001: HALLO ' + req.params.country);

    let cities = await modCity.getCities({countrycode: req.params.countrycode}, {sort: {name: 1}});
    res.json(cities);
});


module.exports = router;
