"use strict";
import {$} from "./nQuery.js";

const countryFullTable = function(country){

}

const countrySimpleTable = function(country){

    console.log(country);
    let table = document.createElement('table');
    table.setAttribute("id", 'countryTable');

    let tableHeader = document.createElement('th')
    let headerName = document.createTextNode("Country Name");
    let rowOne = document.createElement('tr');
    
    tableHeader.appendChild(document.createElement('td').appendChild(headerName));
    rowOne.appendChild(document.createElement('td').appendChild(document.createTextNode(country.name)));

    table.appendChild(tableHeader);
    table.appendChild(rowOne);


    return table;
}

export{countryFullTable, countrySimpleTable};