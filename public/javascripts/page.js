"use strict";
import {$} from "./modules/nQuery.js";
import {Ajax} from "./modules/Ajax.js";
import { countryFullTable, countrySimpleTable } from "./modules/countryDetails.js";

/*
 * Event handler for button - create ajax object and get data
 */
const getContinents = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile("/continents", showContinents);
};
const getCountries = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile(`/continents/${ev.target.value}`, showCountries);
};
const getCountryData = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile(`/country/${ev.target.value}`, showCountry);
};
const getCities = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    console.log('5000:' + ev.target.value);
    req.getFile(`/cities/${ev.target.value}`, showCities);
};


/*
 * callback function for the above AJaX
 */
const showContinents = function(e) {
    /*
     * here you put the ajax response onto your page DOM
     */
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("contdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Continents');
    h3.appendChild(txt);
    div.appendChild(h3);
    let continents = JSON.parse(e.target.responseText);
    let sel = document.createElement('select');
    sel.setAttribute('id', 'chooseContinent');
    sel.addEventListener('change', getCountries);
    continents.forEach(function(continent) {
        let opt = document.createElement('option');
        let opttext = document.createTextNode(continent.name);
        opt.appendChild(opttext);
        sel.appendChild(opt);
    });
    div.appendChild(sel);
    $("contdata").appendChild(div);
}

const showCountries = function (e) {
    /*
     * here you put the ajax response onto your page DOM
     */
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("countdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Countries');
    h3.appendChild(txt);
    div.appendChild(h3);
    let countries = JSON.parse(e.target.responseText);

    let sel = document.createElement('select');
    sel.setAttribute('id', 'chooseCountry');
    //  sel.addEventListener('change', getCountryData);
     sel.addEventListener('change', getCities);
    countries.forEach(function(country) {
        let opt = document.createElement('option');
        opt.value = country.code; //TODO: INSERT VALUE HERE !!!
        let opttext = document.createTextNode(country.name);
        opt.appendChild(opttext);
        sel.appendChild(opt);
        
    });

    // *** Add an element to contain the data about the selected country
    let section = document.createElement("section");
    section.setAttribute("id", 'countryData');
    
    div.appendChild(sel);
    div.appendChild(section);
    $("countdata").appendChild(div);
    

};
const showCountry = function (e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("countryData");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Data');
    h3.appendChild(txt);
    div.appendChild(h3);
    let country = JSON.parse(e.target.responseText);
    console.log(country);

    let table = countrySimpleTable(country[0]);
    div.appendChild(table);

    $("countryData").appendChild(div);
}

const showCities = function(e) {
    /*
     * here you put the ajax response onto your page DOM
     */
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("citydata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Cities');
    h3.appendChild(txt);
    div.appendChild(h3);
    let cities = JSON.parse(e.target.responseText);
    let sel = document.createElement('select');
    sel.setAttribute('id', 'chooseCity');
    // sel.addEventListener('change', getCountries);
    cities.forEach(function(city) {
        let opt = document.createElement('option');
        let opttext = document.createTextNode(city.name);
        opt.appendChild(opttext);
        sel.appendChild(opt);
    });
    div.appendChild(sel);
    $("citydata").appendChild(div);
}

/*
 *  Listen to the get films button
 */
const showStarter = function () {
    $('gcont').addEventListener('click', getContinents);
}

window.addEventListener("load", showStarter);                   // kick off JS
